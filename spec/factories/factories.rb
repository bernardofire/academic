FactoryGirl.define do
  factory :student do
    name 'John'
  end

  factory :course do
    name 'science'
    description 'wat'
  end
end
