require 'rails_helper'

describe Student do
  describe 'status' do
    it 'should be created with status 1' do
      student = create(:student)
      expect(student.status).to be_eql(1)
    end

    it 'active?' do
      student = create(:student)
      expect(student.active?).to be_eql('Yes')
      student.status = 0
      expect(student.active?).to be_eql('No')
    end

    it 'deactivate' do
      student = create(:student)
      expect(student.active?).to be_eql('Yes')
      student.deactivate
      expect(student.active?).to be_eql('No')
    end

    it 'activate' do
      student = create(:student)
      student.deactivate
      student.activate
      expect(student.active?).to be_eql('Yes')
    end
  end

  it { should validate_presence_of(:name) }

  it 'should set register_number properly' do
    student = Student.create(name: 'John')
    expect(student.register_number).to be_eql(student.id.to_s)
  end

  it 'has many courses' do
    student = create(:student)
    student.courses << create(:course)
    expect(student.courses.count).to be_eql(1)
    student.courses << create(:course)
    expect(student.courses.count).to be_eql(2)
  end
end
