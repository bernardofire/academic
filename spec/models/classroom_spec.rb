require 'rails_helper'

describe Classroom do
  it 'set entry_at when created' do
    course = create(:course)
    student = create(:student)
    classroom = Classroom.create(course: course, student: student)
    expect(classroom.entry_at.hour).to be_eql(DateTime.now.hour)
  end
end
