require 'rails_helper'

describe Course do
  describe 'status' do
    it 'should be created with status 1' do
      course = create(:course)
      expect(course.status).to be_eql(1)
    end

    it 'active?' do
      course = create(:course)
      expect(course.active?).to be_eql('Yes')
      course.status = 0
      expect(course.active?).to be_eql('No')
    end

    it 'deactivate' do
      course = create(:course)
      expect(course.active?).to be_eql('Yes')
      course.deactivate
      expect(course.active?).to be_eql('No')
    end

    it 'activate' do
      course = create(:course)
      course.deactivate
      course.activate
      expect(course.active?).to be_eql('Yes')
    end
  end
  it { should validate_presence_of(:name) }

  it 'should allow description' do
    course = Course.create(name: 'Science', description: 'wat')
    expect(course.description).to be_eql('wat')
  end

  it 'has many students' do
    course = create(:course)
    course.students << create(:student)
    expect(course.students.count).to be_eql(1)
    course.students << create(:student)
    expect(course.students.count).to be_eql(2)
  end
end
