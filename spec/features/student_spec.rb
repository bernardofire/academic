require 'rails_helper'

feature Student do
  before do
    @student = create(:student)
    @course = create(:course)
  end
  context 'new' do
    scenario 'successfully' do
      visit students_path
      click_link 'New'
      fill_in 'Name', with: 'goku'
      find(:css, '[type="checkbox"]').set(true)
      click_button 'Create Student'
      expect(page).to have_content('Student was successfully created.')
      expect(page).to have_content('goku')
    end
  end

  context 'edit' do
    scenario 'successfully' do
      visit edit_student_path(@student)
      fill_in 'Name', with: 'whatever'
      click_button 'Update Student'
      expect(page).to have_content('Student was successfully updated.')
      expect(page).to have_content('whatever')
    end
  end

  context 'show' do
    scenario 'no courses' do
      visit student_path(@student)
      expect(page).to have_content(@student.name)
      expect(page).to have_content(@student.active?)
    end

    scenario 'with courses' do
      student = create(:student)
      course = create(:course)
      Classroom.create(course: course, student: student)
      visit student_path(student)
      expect(page).to have_content(student.name)
      expect(page).to have_content(student.active?)
      expect(page).to have_content(course.name)
    end
  end

  context 'status' do
    scenario 'deactivate'do
      visit student_path(@student)
      expect(page).to have_content('Yes')
      click_link 'Deactivate'
      expect(page).to have_content('No')
    end

    scenario 'activate'do
      visit student_path(@student)
      click_link 'Deactivate'
      expect(page).to have_content('No')
      click_link 'Activate!'
      expect(page).to have_content('Yes')
    end
  end
end

