require 'rails_helper'

feature Course do
  before do
    @student = create(:student)
    @course = create(:course)
  end
  context 'new' do
    scenario 'successfully' do
      visit courses_path
      click_link 'New'
      fill_in 'Name', with: 'pixar'
      fill_in 'Description', with: 'shh'
      find(:css, '[type="checkbox"]').set(true)
      click_button 'Create Course'
      expect(page).to have_content('Course was successfully created.')
      expect(page).to have_content('pixar')
      expect(page).to have_content('shh')
    end
  end

  context 'edit' do
    scenario 'successfully' do
      visit edit_course_path(@course)
      fill_in 'Name', with: 'whatever'
      fill_in 'Description', with: 'may'
      click_button 'Update Course'
      expect(page).to have_content('Course was successfully updated.')
      expect(page).to have_content('whatever')
      expect(page).to have_content('may')
    end
  end

  context 'show' do
    scenario 'no students' do
      visit course_path(@course)
      expect(page).to have_content(@course.name)
      expect(page).to have_content(@course.description)
      expect(page).to have_content(@course.active?)
    end

    scenario 'with students' do
      student = create(:student)
      course = create(:course)
      Classroom.create(student: student, course: course)
      visit course_path(course)
      expect(page).to have_content(course.name)
      expect(page).to have_content(course.description)
      expect(page).to have_content(course.active?)
      expect(page).to have_content(student.name)
    end
  end

  context 'status' do
    scenario 'deactivate'do
      visit course_path(@course)
      expect(page).to have_content('Yes')
      click_link 'Deactivate'
      expect(page).to have_content('No')
    end

    scenario 'activate'do
      visit course_path(@course)
      click_link 'Deactivate'
      expect(page).to have_content('No')
      click_link 'Activate!'
      expect(page).to have_content('Yes')
    end
  end
end
