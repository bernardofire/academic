class Student < ActiveRecord::Base
  has_many :courses, through: :classrooms
  has_many :classrooms
  validates :name, presence: true
  before_create :set_status
  include SharedMethods

  def register_number
    self.id.to_s
  end
end
