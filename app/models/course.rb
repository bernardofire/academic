class Course < ActiveRecord::Base
  has_many :students, through: :classrooms
  has_many :classrooms
  validates :name, presence: true
  before_create :set_status
  include SharedMethods
end
