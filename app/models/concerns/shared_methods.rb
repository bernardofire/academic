module SharedMethods
  def set_status
    self.status = 1
  end

  def activate
    self.status = 1
  end

  def deactivate
    self.status = 0
  end

  def active?
    return 'Yes' if self.status == 1
    'No'
  end
end
