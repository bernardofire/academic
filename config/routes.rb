Rails.application.routes.draw do
  root to: 'pages#home'
  resources :courses do
    get '/activate', to: 'courses#activate'
    get '/deactivate', to: 'courses#deactivate'
  end
  resources :students do
    get '/activate', to: 'students#activate'
    get '/deactivate', to: 'students#deactivate'
  end
end
